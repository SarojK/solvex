import React from 'react';
import logo1 from '../assets/logo1.jpeg';
import logo2 from '../assets/logo2.jpeg';
import logo3 from '../assets/logo3.jpeg';

const options=[
    {value: "1", image: logo1},
    {value: "2", image: logo2},
    {value: "3", image: logo3}
];

function Question1 (props) {
    const {value1,value2,value3 } = props.data;
    console.log('value1 :>> ', value1);

    return(
        <div>
        <div className="row"><h4>1. Which of these designs best represents a consumer company which is closely connected to the farmers? (Rate 1-5)</h4></div>
        <div className="row" onChange={(e) => {props.handleChangeQuestion(e, "question1")}}>
            {options.map((item, index)=><div><input type="radio" checked={item.value===value1} value={item.value} name="value1" key={index}/><label style={{paddingRight: "10px"}}>Option {item.value}</label><img src={item.image} className="App-logo" alt=""/></div>)}
        </div>

        </div>
    )
}

export default Question1;