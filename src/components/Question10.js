import React from 'react';
import Logo1 from '../assets/logo1.jpeg';


function Question10 (props) {
    return(
        <div>
        <div className="row"><img src={Logo1} className="App-logo" alt=""/></div>
        <div className="row">
        <textarea name="question10" className="textarea"
        rows="5"
          onChange={props.handleChangeQuestion10}
          value={props.question10.value}
          placeholder='Type text here....'/>
          </div>
        </div>
    )
}

export default Question10;