import React from 'react';
import Question1 from './Question1';
import Question2 from './Question2';
import Question4 from './Question4';
import Question8 from './Question8';
import Question9 from './Question9';
import Question10 from './Question10';
import Question11 from './Question11';
import Question12 from './Question12';
import Question13 from './Question13';
import Logo1 from '../assets/logo1.jpeg';
import Logo2 from '../assets/logo2.jpeg';
import Logo3 from '../assets/logo3.jpeg';
import api from "../Service/Api";
import backend from "../Service/Backend";
import { isCompositeComponent } from 'react-dom/test-utils';

const section1Options=["","1","2","3","4","5","6","7","8","9","10"];

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            currentQuestion: 1,
            totalQuestions: 5,
            currentSection: 1,
            totalSections: 3,
            section1 : {
                logo1: "",
                logo2: "",
                logo3: ""
            },
            question1 : {
                value1: "",
                value2: "",
                value3: "",
            },
            question2: {
                value1: "",
                value2: "",
                value3: "",
            },
            question4: {
                value1: "",
                value2: "",
                value3: "",
            },
            question8: {
                value1: "",
                value2: "",
                value3: "",
            },
            question9: {
                value1: "",
                value2: "",
                value3: "",
            },
            question10: {
                value: ""
            },
            question11: {
                value: ""
            },
            question12: {
                value: ""
            },
            question13: {
                value: ""
            }
        }
    }

    nextBtnClick = () => {
        this.setState({ currentQuestion: this.state.currentQuestion + 1 });
    }

    handleChangeQuestion = (e, quesNumber) => {
        console.log('quesNumber :>> ', quesNumber);
        console.log('e.target.name :>> ', e.target.name);
        console.log('e.target.value :>> ', e.target.value);
        if(quesNumber === "question1"){
            this.setState({
                question1: {
                    ...this.state.question1,
                    value1: e.target.value
                }
            })
        }
        else {this.setState({
            [quesNumber]: {
                ...this.state[quesNumber],
                [e.target.name]: e.target.value
            }
        })}
    }

    handleChangeQuestion10 = (e) => {
        this.setState({
            question10: { ...this.state.question10, value: e.target.value }
        })
    }

    handleChangeQuestion11 = (e) => {
        this.setState({
            question11: { ...this.state.question11, value: e.target.value }
        })
    }

    handleChangeQuestion12 = (e) => {
        this.setState({
            question12: { ...this.state.question12, value: e.target.value }
        })
    }

    handleChangeQuestion13 = (e) => {
        this.setState({
            question13: { ...this.state.question13, value: e.target.value }
        })
    }

    submitBtnClick = async() => {
        const { question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11, question12, question13 } = this.state;
        const input = {
            question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11, question12, question13
        }
        console.log('input :>> ', input);

        const data = {
            api: api.survey.create ,
            payLoad: JSON.stringify(input)
        }

        console.log("data: ", data)

        const response = await backend.save(data);
        console.log("Response : ", response);
    }

    submitSection1BtnClick = async () => {
        const { section1 } = this.state;
        const input = {
            section1
        }
        console.log('input :>> ', input);
        const data = {
            api: api.survey.create ,
            payLoad: JSON.stringify(input)
        }
        console.log("data: ", data)
        this.setState({currentSection : 2})
        const response = await backend.save(data);
        console.log("Response : ", response);
    }

    submitSection2BtnClick = async () => {
        const { question1, question2, question4, question8, question9} = this.state;
        const input = {
            question1, question2, question3 : question4, question4 :question8, question5: question9
        }
        console.log('input :>> ', input);
        const data = {
            api: api.survey.create ,
            payLoad: JSON.stringify(input)
        }
        console.log("data: ", data)
        this.setState({currentSection : 3})
        const response = await backend.save(data);
        console.log("Response : ", response);
    }

    submitSection3BtnClick = async () => {
        const { question10,question11,question12} = this.state;
        const input = {
            question6: question10,question7: question11,question8: question12
        }
        console.log('input :>> ', input);
        const data = {
            api: api.survey.create ,
            payLoad: JSON.stringify(input)
        }
        console.log("data: ", data)
        this.setState({currentSection : 4})
        const response = await backend.save(data);
        console.log("Response : ", response);
    }

    setSection1Q1 =(e) => {
        this.setState({section1: {...this.state.section1, logo1: e.target.value }})
    }

    setSection1Q2 =(e) => {
        this.setState({section1: {...this.state.section1, logo2: e.target.value }})
    }

    setSection1Q3 =(e) => {
        this.setState({section1: {...this.state.section1, logo3: e.target.value }})
    }

    section1 = () => {
        const {logo1, logo2, logo3} = this.state.section1;
        return (
        <div>
            <div className="row"><h2>Section 1 of 3</h2> <h4>Rate these logos as per your liking (Only this section will be Mandatory)</h4></div>
            <div className="row">
                <img src={Logo1} className="App-logo" alt=""/>
            </div>
            <div className="row">
                <select className="dropdown" name="value1" value={logo1} onChange={this.setSection1Q1}>
                    {section1Options.map(item=>{
                return <option value={item}>{item}</option>
            })}
        </select>
            </div>
            <div className="row">
                <img src={Logo2} className="App-logo" alt=""/>
            </div>
            <div className="row">
                <select className="dropdown" name="value2" value={logo2} onChange={this.setSection1Q2}>
                    {section1Options.map(item=>{
                return <option value={item}>{item}</option>
            })}
        </select>
            </div>
            <div className="row">
                <img src={Logo3} className="App-logo3" alt=""/>
            </div>
            <div className="row" onChange={this.setSection1Q3}>
                <select className="dropdown" name="value3" value={logo3} onChange={this.setSection1Q3}>
                    {section1Options.map(item=>{
                return <option value={item}>{item}</option>
            })}
        </select>
            </div>
            <br /><br />
            <div className="row">
                <button className="next-button" 
                disabled={logo1==="" ||logo2==="" || logo3===""} 
                onClick={()=>{
                    this.submitSection1BtnClick();
                    // this.setState({currentSection : 2})
                    }}>Submit</button>
            </div>
            <br />
        </div>
            );
    }

    renserSection2Questions = () => {
        const {question1,question2, question4, question9 } = this.state;
        return(<React.Fragment>
            <Question1 data={question1} handleChangeQuestion={this.handleChangeQuestion}/>
            <Question2 data={question2} handleChangeQuestion={this.handleChangeQuestion}/>
            <Question4 data={question4} handleChangeQuestion={this.handleChangeQuestion}/>
            {/* <Question8 data={question8} handleChangeQuestion={this.handleChangeQuestion}/> */}
            <Question9 submitSection2BtnClick={this.submitSection2BtnClick} data={question9} handleChangeQuestion={this.handleChangeQuestion}/>
            <br />
            <br />
            <div className="row">
                    <button className="next-button" onClick={this.submitSection2BtnClick}>Submit</button>
                </div>
                <br />
            </React.Fragment>
        );  
    }
    section2 = () => {
        return (
            <div>
                <div className="row"><h2>Section 2 of 3 </h2></div>
                {this.renserSection2Questions()}
            </div>
                );
    }

    section3 = () => {
        const {question10, question11, question12, question13} = this.state;
        return (
            <div>
                <div className="row"><h2> Section 3 of 3 </h2> <h3>Your Thoughts</h3></div>
                    <Question10 question10={question10} handleChangeQuestion10={this.handleChangeQuestion10}/>
                    <Question11 question11={question11} handleChangeQuestion11={this.handleChangeQuestion11}/>
                    <Question12 question12={question12} handleChangeQuestion12={this.handleChangeQuestion12}/>
                    {/* <Question13 question13={question13} handleChangeQuestion13={this.handleChangeQuestion13}/> */}
                    <br />
            <br />
                <div className="row">
                    <button className="next-button" onClick={this.submitSection3BtnClick}>Submit</button>
                </div>
                <br />
            </div>
                );
    }

    section4 = () => {
        return (
            <div>
                <h2>Thanks for submitting the survey!</h2>
            </div>
        )
    }
    
    renderQuestions =() => {
        const {currentSection} = this.state;
        console.log('currentSection :>> ', currentSection);
        switch(currentSection){
            case 1: return this.section1();
            case 2: return this.section2();
            case 3: return this.section3();
            case 4: return this.section4();
            default: return null;
        }
    }

    render() {
        return (
            <div>
                <div className="row">
                {/* <h3>Survey <label style={{float: "right" }}>{currentQuestion.toString()}/{totalQuestions.toString()}</label></h3> */}
                </div>
                <div>
                    {this.renderQuestions()}
                </div>
            </div>
        )
    }
}