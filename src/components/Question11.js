import React from 'react';
import Logo2 from '../assets/logo2.jpeg';


function Question11 (props) {
    return(
        <div>
        <div className="row"><img src={Logo2} className="App-logo" alt=""/></div>
        <div className="row">
        <textarea name="question10" className="textarea"
        rows="5"
          onChange={props.handleChangeQuestion11}
          value={props.question11.value}
          placeholder='Type text here....'/>
          </div>
        
        </div>
    )
}

export default Question11;