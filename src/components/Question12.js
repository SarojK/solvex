import React from 'react';
import Logo3 from '../assets/logo3.jpeg';


function Question12 (props) {
    return(
        <div>
        <div className="row"><img src={Logo3} className="App-logo3" alt=""/></div>
        <div className="row">
        <textarea name="question10" className="textarea"
        rows="5"
          onChange={props.handleChangeQuestion12}
          value={props.question12.value}
          placeholder='Type text here....'/>
          </div>
        
        </div>
    )
}

export default Question12;