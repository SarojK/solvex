import React from 'react';
import logo1 from '../assets/logo1.jpeg';
import logo2 from '../assets/logo2.jpeg';
import logo3 from '../assets/logo3.jpeg';

const options=["Budget Friendly","Mid-range (value for money)","Premium"];

function Question4 (props) {
    const {value1,value2,value3 } = props.data;
    return(
        <div>
        <div className="row"><h4>3. What price segment do you think the company products belong to w.r.t these 3 logos?</h4></div>
        
        <div className="row">
        <img src={logo1} className="App-logo" alt=""/>
        </div>
        <div className="row" onChange={(e) => {props.handleChangeQuestion(e, "question4")}}>
        {options.map((item, index)=><div><input type="radio" checked={item===value1} value={item} name="value1" key={index}/> <label style={{paddingRight: "10px"}}>{item}</label></div>)}
        </div>

        <div className="row">
        <img src={logo2} className="App-logo" alt=""/>
        </div>
        <div className="row" onChange={(e) => {props.handleChangeQuestion(e, "question4")}}>
        {options.map((item, index)=><div><input type="radio" checked={item===value2} value={item} name="value2" key={index}/> <label style={{paddingRight: "10px"}}>{item}</label></div>)}
        </div>

        <div className="row">
        <img src={logo3} className="App-logo3" alt=""/>
        </div>
        <div className="row" onChange={(e) => {props.handleChangeQuestion(e, "question4")}}>
        {options.map((item, index)=><div><input type="radio" checked={item===value3} value={item} name="value3" key={index}/> <label style={{paddingRight: "10px"}}>{item}</label></div>)}
        </div>

        </div>
    )
}

export default Question4;