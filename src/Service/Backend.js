const axios = require('axios');
const api = require('./Api')

let { baseUrl } = api

console.log("base url :\n", baseUrl);

const axiosInstance = axios.create({
    baseURL: baseUrl,
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
    }
});

const get = async (data) => {
    try {
        if (!data) { throw new Error(`No Data`) }
        if (data && !data.endPoint) { throw new Error(`No API Endpoint`) }

        if (data.authToken) {
            axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + data.authToken;
        }

        // End Point
        let endPoint = data.endPoint
        if (data.urlParam) {
            endPoint = endPoint + "/" + data.urlParam
        }

        //showId(`endpoint : ${endPoint}`)

        const response = await axiosInstance.get(endPoint, { params: data.queryParam })
        if (response && response.data && response.data.status === 'success') {
            const { data } = response.data
            return data
        } else {
            // Ideally we should not come here if response is structured properly
            return response
        }
    } catch (error) {
        console.log('Error while getting data');

        if (error.response) {
            let e = {
                errMessage: 'Error while getting data',
                data: null,
                errCode: null
            }

            const errorResponse = error.response
            if (errorResponse.data) {
                if (errorResponse.data.status === 'error') {
                    e.errMessage = errorResponse.data.message;
                    e.data = errorResponse.data.data;
                    e.errCode = errorResponse.data.code;
                }
            }

            throw e;
        } else {
            console.error(error)
            throw error;
        }
    }
}

const post = async (data) => {
    try {
        if (!data) { throw new Error(`No Data`) }
        if (data && !data.endPoint) { throw new Error(`No API Endpoint`) }

        if (data.authToken) {
            axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + data.authToken;
        }

        // End Point
        const endPoint = data.endPoint
        // Payload
        let payLoad
        if (data.payLoad) {
            payLoad = data.payLoad
        }

        //console.log(endPoint)

        const response = await axiosInstance.post(endPoint, payLoad)
        if (response && response.data && response.data.status === 'success') {
            const { data } = response.data
            return data
        } else {
            return response
        }
    } catch (error) {
        console.log('Error while posting')

        if (error.response) {
            let e = {
                errMessage: 'Error while posting data',
                data: null,
                errCode: null
            }

            const errorResponse = error.response
            if (errorResponse.data) {
                if (errorResponse.data.status === 'error') {
                    e.errMessage = errorResponse.data.message;
                    e.data = errorResponse.data.data;
                    e.errCode = errorResponse.data.code;
                }
            }

            throw e;
        } else {
            console.error(error)
            throw error;
        }
    }
}

const save = async (data, protectedRoute) => {
    try {
        // Pre Validation
        if (!data) { throw new Error(`No Data`) }
        if (data && !data.api) { throw new Error(`No API Details`) }
        if (data && data.api && !data.api.method) { throw new Error(`No API Method`) }

        const baseEndPoint = `https://us-central1-solvex-5b976.cloudfunctions.net/app`;
        let endPoint = baseEndPoint + data.api.endPoint

        const d = {
            endPoint: endPoint,
            urlParam: data.urlParam ? data.urlParam : null,
            payLoad: data.payLoad ? data.payLoad : null,
            authToken: null
        }

        if (data.api.method === 'POST') {
            return await post(d)
        }

    } catch (err) {
        console.log('error at save')
        throw err;
    }
}

const fetch = async (data) => {
    try {
        // Pre Validation
        if (!data) { throw new Error(`No Data`) }
        if (data && !data.api) { throw new Error(`No API Details`) }
        if (data && data.api && !data.api.method) { throw new Error(`No API Method`) }

        const baseEndPoint = data.api.baseEndPoint
        let endPoint = baseEndPoint + data.api.endPoint

        const d = {
            endPoint: endPoint,
            urlParam: data.urlParam ? data.urlParam : null,
            queryParam: data.queryParam ? data.queryParam : null,
            authToken: null
        }

        if (data.api.method === 'GET') {
            return await get(d)
        }
    } catch (err) {
        console.log('error at fetch')
        throw err;
    }
}

const backend = { save, fetch }

export default backend