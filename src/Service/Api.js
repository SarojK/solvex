/* API Base and EndPoints */

let baseUrl = "https://us-central1-solvex-5b976.cloudfunctions.net"

const test = {
    serverStatus: {
        method: 'GET',
        baseEndPoint: '/app',
        endPoint: '/test/serverstatus'
    }
}

const survey = {
    create: {
        method: 'POST',
        baseEndPoint: '/app',
        endPoint: '/survey/savesurvey'
    }
}

const api = { baseUrl, test, survey }

export default api