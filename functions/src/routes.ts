import { Application } from 'express'
import testRouter from './test/routes'
import surveyRouter from './survey/routes'

export const routesConfig = (app: Application) => {
  app.use('/test', testRouter)
  app.use('/survey', surveyRouter)
}
