/* Firestore database */

export let db: FirebaseFirestore.Firestore
export const init = (database: FirebaseFirestore.Firestore) => {
    db = database
}

/* DB Collections Name */

export const enum COLLECTION {
    RECORDS = 'Records'
}
