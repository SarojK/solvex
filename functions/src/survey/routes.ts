import { Router, Request, Response } from 'express'
import { AppError } from '../utils/response';
import { db } from '../utils/firestore'

const router = Router()

router.post('/savesurvey', async (req: Request, res: Response) => {
    try {
        const obj: any = req.body;

        const newRecordsDocRef: FirebaseFirestore.CollectionReference = db.collection('records')

        const record = await newRecordsDocRef.add(obj)

        // Error check
        if (!record) {
            console.log('error : ')
            return handleError(res, 'Saving in DB is failed')
        }

        return res.status(200).json({
            data: {
                message: 'Survey is successfully saved in the DB!!'
            }
        })
    } catch (err) {
        console.log('error : ', err)
        return handleError(res, err)
    }
})

const handleError = async (res: Response, err: any) => {
    const err_response: AppError = {
        status: 'error',
        code: 500,
        message: 'Internal Server Error. Please Try again !',
        data: null
    }

    if (err.code) { err_response.code = err.code }
    if (err.message) { err_response.message = err.message }
    if (err.data) { err_response.data = err.data }

    console.error('USER : ERROR ERROR ERROR !!')
    console.error(err_response)

    return res.status(500).json(err_response)
}

export default router
